package space.idan.fidonews

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import org.koin.androidx.viewmodel.ext.android.viewModel
import space.idan.fidonews.domain.model.Screen
import space.idan.fidonews.presentation.App
import space.idan.fidonews.presentation.NavigationViewModel
import space.idan.fidonews.presentation.feed.FeedEvent
import space.idan.fidonews.presentation.feed.FeedViewModel
import space.idan.fidonews.presentation.ui.theme.FidoNewsTheme

class MainActivity : ComponentActivity() {
    private val feedViewModel: FeedViewModel by viewModel()
    private val navigationViewModel: NavigationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FidoNewsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    App(
                        navigationViewModel = navigationViewModel,
                        feedViewModel = feedViewModel,
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (navigationViewModel.screen.value is Screen.Feed) {
            feedViewModel.handle(FeedEvent.LoadTopHeadlines())
        }
    }
}