package space.idan.fidonews.presentation.ui.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import space.idan.fidonews.domain.model.Source
import space.idan.fidonews.presentation.ui.theme.FidoNewsTheme

@Composable
fun SourceTile(
    source: Source,
    onClick: (Source) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        shape = RoundedCornerShape(4.dp),
        elevation = 2.dp,
        modifier = modifier
            .clickable {
                onClick(source)
            }
    ) {
        Text(
            source.name,
            color = MaterialTheme.colors.onSurface.takeIf { source.isInclude.not() } ?: MaterialTheme.colors.primary,
            textAlign = TextAlign.Center,
            modifier = Modifier.
                padding(
                    horizontal = 8.dp,
                    vertical = 4.dp,
                )
        )
    }
}

@Preview
@Composable
private fun PreviewSourceTile() {
    FidoNewsTheme {
        SourceTile(
            source = Source(id = "wire", name = "Wired", isInclude = true),
            onClick = {}
        )
    }
}