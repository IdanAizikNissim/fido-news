package space.idan.fidonews.presentation.ui.screen

import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidView
import space.idan.fidonews.domain.model.Article

@Composable
fun ArticleScreen(article: Article) {
    AndroidView(
        factory = { context ->
            WebView(context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                )

                webViewClient = WebViewClient()
            }
        },
        update = {
            it.loadUrl(article.url)
        }
    )
}