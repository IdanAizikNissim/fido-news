package space.idan.fidonews.presentation.feed

import space.idan.fidonews.domain.model.Source

sealed interface FeedEvent {
    data class OnSourceClicked(val source: Source) : FeedEvent
    class LoadTopHeadlines(val refreshSources: Boolean = false) : FeedEvent
}