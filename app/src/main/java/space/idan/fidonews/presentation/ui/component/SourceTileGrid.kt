package space.idan.fidonews.presentation.ui.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import space.idan.fidonews.domain.model.Source
import space.idan.fidonews.presentation.ui.theme.FidoNewsTheme

@Composable
fun SourceTileGrid(
    sources: List<Source>,
    onSourceTileClicked: (Source) -> Unit,
    modifier: Modifier = Modifier,
) {
    LazyRow (
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight(),
    ) {
        items(sources) { source ->
            SourceTile(
                source = source,
                onClick = onSourceTileClicked,
                modifier = Modifier
                    .padding(4.dp)
            )
        }
    }
}


@Preview
@Composable
private fun PreviewSourceTileGrid() {
    val sources = (0 until 20).map { Source(id = "wire", name = "Wired", isInclude = false) }
    FidoNewsTheme {
        SourceTileGrid(
            sources = sources,
            onSourceTileClicked = {},
        )
    }
}