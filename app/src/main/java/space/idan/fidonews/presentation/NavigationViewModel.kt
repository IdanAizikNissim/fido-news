package space.idan.fidonews.presentation

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import space.idan.fidonews.domain.model.Screen

class NavigationViewModel : ViewModel() {
    private val _screen = MutableStateFlow<Screen>(Screen.Feed)
    val screen: MutableStateFlow<Screen> = _screen

    fun navigateTo(screen: Screen) {
        if (screen != _screen.value) {
            _screen.value = screen
        }
    }

    fun pop() {
        if (_screen.value is Screen.News) {
            _screen.value = Screen.Feed
        }
    }
}

