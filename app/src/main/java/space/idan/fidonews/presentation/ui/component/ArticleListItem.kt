package space.idan.fidonews.presentation.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.joda.time.DateTime
import space.idan.fidonews.domain.model.Article
import space.idan.fidonews.presentation.ui.theme.FidoNewsTheme
import space.idan.fidonews.presentation.ui.theme.Gradient

@Composable
fun ArticleListItem(
    article: Article,
    modifier: Modifier = Modifier,
) {
    Row(
        horizontalArrangement = Arrangement.SpaceAround,
        modifier = modifier
            .fillMaxWidth()
            .heightIn(72.dp, 96.dp)
            .background(MaterialTheme.colors.surface)
    ) {
        ArticleImage(
            icon = article.icon,
            modifier = Modifier.padding(6.dp)
        )
        ArticleContent(
            title = article.title,
            source = article.source,
            publishedAt = article.publishedAt
        )
    }
}

@Composable
private fun ArticleImage(
    icon: Gradient,
    modifier: Modifier = Modifier,
) {
    Box(
        modifier = modifier
            .fillMaxHeight()
            .width(72.dp)
            .clip(RoundedCornerShape(4.dp))
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        icon.from,
                        icon.to
                    )
                )
            )
    )
}

@Composable
private fun ArticleContent(
    title: String,
    source: String,
    publishedAt: String,
    modifier: Modifier = Modifier,
) {
    Column(
        verticalArrangement = Arrangement.SpaceAround,
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = 4.dp)
    ) {
        Text(
            title,
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp,
            style = MaterialTheme.typography.h3
        )
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = modifier
                .fillMaxWidth()
                .padding(
                    end = 2.dp
                )
        ) {
            Text(
                source
            )

            Text(
                publishedAt
            )
        }
    }
}

@Preview
@Composable
private fun PreviewArticleListItem() {
    FidoNewsTheme {
        ArticleListItem(
            article = Article(
                title = "The latest on the Ukraine-Russia border crisis: Live updates",
                description = "Tensions between Moscow and Kyiv are at their highest in years, with a large Russian troop buildup near the shared borders of the two former Soviet republics. Follow here for the latest news updates.",
                source = "CNN",
                url = "https://www.cnn.com/europe/live-news/ukraine-russia-news-02-20-22-intl/index.html",
                dateTime = DateTime.now(),
                publishedAt = "20 minutes ago",
                icon = Gradient.all[0],
            )
        )
    }
}
