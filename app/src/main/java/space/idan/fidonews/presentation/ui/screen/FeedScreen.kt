package space.idan.fidonews.presentation.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import space.idan.fidonews.domain.model.Article
import space.idan.fidonews.presentation.feed.FeedEvent
import space.idan.fidonews.presentation.feed.FeedViewModel
import space.idan.fidonews.presentation.ui.component.ArticleList
import space.idan.fidonews.presentation.ui.component.SourceTileGrid

@Composable
fun FeedScreen(
    viewModel: FeedViewModel,
    onArticleClick: (Article) -> Unit,
) {
    val state = viewModel.state.collectAsState()

    SwipeRefresh(
        state = rememberSwipeRefreshState(state.value.isLoading),
        onRefresh = {
            viewModel.handle(FeedEvent.LoadTopHeadlines())
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            SourceTileGrid(
                sources = state.value.sources.toList(),
                onSourceTileClicked = { source ->
                    viewModel.handle(FeedEvent.OnSourceClicked(source))
                }
            )

            ArticleList(
                articles = state.value.articles,
                onArticleClick = onArticleClick
            )
        }
    }
}