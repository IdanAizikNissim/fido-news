package space.idan.fidonews.presentation.ui.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import org.joda.time.DateTime
import space.idan.fidonews.domain.model.Article
import space.idan.fidonews.presentation.ui.theme.FidoNewsTheme
import space.idan.fidonews.presentation.ui.theme.Gradient

@Composable
fun ArticleList(
    articles: List<Article>,
    onArticleClick: (Article) -> Unit,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier
            .fillMaxSize()
    ) {
        items(articles) { article ->
            ArticleListItem(
                article = article,
                modifier = modifier
                    .padding(
                        vertical = 4.dp,
                    )
                    .clickable {
                        onArticleClick(article)
                    }
            )
        }
    }
}

@Preview
@Composable
private fun PreviewArticleList() {
    val articles = (0 until 20).map {
        Article(
            title = "The latest on the Ukraine-Russia border crisis: Live updates",
            description = "Tensions between Moscow and Kyiv are at their highest in years, with a large Russian troop buildup near the shared borders of the two former Soviet republics. Follow here for the latest news updates.",
            source = "CNN",
            dateTime = DateTime.now(),
            url = "https://www.cnn.com/europe/live-news/ukraine-russia-news-02-20-22-intl/index.html",
            publishedAt = "20 minutes ago",
            icon = Gradient.all[0]
        )
    }

    FidoNewsTheme {
        ArticleList(articles, onArticleClick = {})
    }
}