package space.idan.fidonews.presentation.feed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import space.idan.fidonews.domain.model.FeedState
import space.idan.fidonews.domain.model.Source
import space.idan.fidonews.interactors.GetSources
import space.idan.fidonews.interactors.GetTopHeadlines
import kotlin.math.min

class FeedViewModel(
    private val getTopHeadlines: GetTopHeadlines,
    private val getSources: GetSources,
) : ViewModel() {
    private val _state = MutableStateFlow(FeedState.empty)
    val state: StateFlow<FeedState> = _state

    init {
        handle(FeedEvent.LoadTopHeadlines(refreshSources = true))
    }

    private fun loadTopHeadlines(query: String?, sources: Set<Source>, refreshSources: Boolean) {
        _state.value = _state.value.copy(isLoading = true)

        viewModelScope.launch {
            var newSources = if (refreshSources || sources.isEmpty()) {
                getSources().toSet()
            } else {
                sources
            }.toList()

            if (newSources.isNotEmpty() && !newSources.any { it.isInclude }) {
                newSources = mutableListOf<Source>().apply {
                    add(0, newSources[0].copy(isInclude = true))
                    addAll(newSources.subList(min(1, newSources.size), newSources.size))
                }
            }

            val articles = getTopHeadlines(query, newSources.filter { it.isInclude }.map { it.id })
            _state.value = _state.value.copy(
                articles = articles,
                isLoading = false,
                sources = newSources.toSet()
            )
        }
    }

    private fun onSourceToggle(source: Source) {
        val sources = _state.value.sources.toMutableList()
        var shouldReload = true

        if (source.isInclude) {
            if (sources.count { it.isInclude } > 1) {
                sources[sources.indexOf(source)] = source.copy(isInclude = false)
            } else {
                shouldReload = false
            }
        } else {
            sources[sources.indexOf(source)] = source.copy(isInclude = true)
        }

        if (shouldReload) {
            _state.value = _state.value.copy(sources = sources.toSet())
            loadTopHeadlines(_state.value.query, _state.value.sources, false)
        }
    }

    fun handle(event: FeedEvent) {
        when (event) {
            is FeedEvent.LoadTopHeadlines -> loadTopHeadlines(_state.value.query, _state.value.sources, event.refreshSources)
            is FeedEvent.OnSourceClicked -> onSourceToggle(event.source)
        }
    }
}