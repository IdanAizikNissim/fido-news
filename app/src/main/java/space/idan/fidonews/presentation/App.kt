package space.idan.fidonews.presentation

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.flow.collectLatest
import space.idan.fidonews.domain.model.Screen
import space.idan.fidonews.presentation.feed.FeedEvent
import space.idan.fidonews.presentation.feed.FeedViewModel
import space.idan.fidonews.presentation.ui.component.HeaderPager
import space.idan.fidonews.presentation.ui.screen.ArticleScreen
import space.idan.fidonews.presentation.ui.screen.FeedScreen

@Composable
fun App(
    feedViewModel: FeedViewModel,
    navigationViewModel: NavigationViewModel
) {
    val coroutineScope = rememberCoroutineScope()
    val navController = rememberNavController()
    val screen = navigationViewModel.screen.collectAsState()

    LaunchedEffect(coroutineScope) {
        navigationViewModel.screen.collectLatest { screen ->
            if (screen is Screen.News) {
                navController.navigate(screen.name)
            } else {
                if (navController.currentBackStackEntry?.destination?.route != Screen.FEED) {
                    navController.popBackStack()
                }
            }
        }
    }

    NavHost(navController = navController, startDestination = Screen.FEED) {
        composable(
            route = Screen.FEED
        ) {
            HeaderPager(
                header = "Top headlines"
            ) {
                FeedScreen(
                    viewModel = feedViewModel,
                    onArticleClick = { article ->
                        navigationViewModel.navigateTo(Screen.News(article))
                    }
                )
            }
        }

        composable(
            route = Screen.NEWS
        ) {
            val article =  (screen.value as? Screen.News)?.article
            if (article != null) {
                HeaderPager(
                    header = article.title
                ) {
                    ArticleScreen(article)
                }

                BackHandler {
                    feedViewModel.handle(FeedEvent.LoadTopHeadlines())
                    navigationViewModel.pop()
                }
            }
        }
    }
}