package space.idan.fidonews.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Pink = Color(0xFFFE8DC6)
val PinkVariant = Color(0xFFFED1C7)

val Purple = Color(0xFF7F00ff)
val PurpleVariant = Color(0xFFE100FF)

val Orange = Color(0xFFFBB040)
val OrangeVariant = Color(0xFFF9ED32)

val Blue = Color(0xFF00A1FF)
val BlueVariant = Color(0xFF00FF8F)

data class Gradient(
    val from: Color,
    val to: Color,
) {
    companion object {
        val all = listOf(
            Gradient(
                from = Pink,
                to = PinkVariant,
            ),
            Gradient(
                from = Purple,
                to = PurpleVariant,
            ),
            Gradient(
                from = Orange,
                to = OrangeVariant,
            ),
            Gradient(
                from = Blue,
                to = BlueVariant,
            ),
        )
    }
}