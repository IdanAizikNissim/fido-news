package space.idan.fidonews.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import space.idan.fidonews.BuildConfig
import space.idan.fidonews.datasource.network.NewsApi
import space.idan.fidonews.datasource.network.NewsApiImpl
import space.idan.fidonews.datasource.network.client.HttpClientBuilder
import space.idan.fidonews.datasource.repo.ArticleRepository
import space.idan.fidonews.datasource.repo.ArticleRepositoryImpl
import space.idan.fidonews.datasource.repo.SourceRepository
import space.idan.fidonews.datasource.repo.SourceRepositoryImpl
import space.idan.fidonews.interactors.GetSources
import space.idan.fidonews.interactors.GetTopHeadlines
import space.idan.fidonews.presentation.NavigationViewModel
import space.idan.fidonews.presentation.feed.FeedViewModel

val appModule = module {
    single<NewsApi> {
        val client = HttpClientBuilder()
            .setBaseUrl(BuildConfig.NEWS_API_URL)
            .setApiToken(BuildConfig.NEWS_API_KEY)
            .build()

        NewsApiImpl(client = client)
    }

    single<SourceRepository> { SourceRepositoryImpl(get()) }
    single<ArticleRepository> { ArticleRepositoryImpl(get()) }

    single { GetTopHeadlines(get()) }
    single { GetSources(get()) }

    viewModel {
        FeedViewModel(
            getTopHeadlines = get(),
            getSources = get(),
        )
    }

    viewModel {
        NavigationViewModel()
    }
}