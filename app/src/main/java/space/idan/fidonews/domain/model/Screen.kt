package space.idan.fidonews.domain.model

sealed class Screen(val name: String) {
    object Feed : Screen(FEED)
    data class News(val article: Article) : Screen(NEWS)

    companion object {
        const val FEED = "feed"
        const val NEWS = "news"
    }
}