package space.idan.fidonews.domain.model

data class Source(
    val id: String,
    val name: String,
    val isInclude: Boolean,
)
