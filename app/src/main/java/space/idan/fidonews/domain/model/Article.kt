package space.idan.fidonews.domain.model

import org.joda.time.DateTime
import space.idan.fidonews.presentation.ui.theme.Gradient

data class Article(
    val icon: Gradient,
    val title: String,
    val description: String,
    val source: String,
    val url: String,
    val dateTime: DateTime,
    val publishedAt: String,
)