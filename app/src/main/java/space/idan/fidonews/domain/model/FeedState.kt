package space.idan.fidonews.domain.model

data class FeedState(
    val isLoading: Boolean,
    val sources: Set<Source>,
    val query: String?,
    val articles: List<Article>,
) {
    companion object {
        val empty = FeedState(
            isLoading = true,
            sources = emptySet(),
            query = null,
            articles = emptyList(),
        )
    }
}
