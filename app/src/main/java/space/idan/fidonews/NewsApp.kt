package space.idan.fidonews

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import space.idan.fidonews.di.appModule

class NewsApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger()
            androidContext(this@NewsApp)
            modules(appModule)
        }
    }
}