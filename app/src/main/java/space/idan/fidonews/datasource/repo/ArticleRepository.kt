package space.idan.fidonews.datasource.repo

import space.idan.fidonews.datasource.network.model.ArticleDto

interface ArticleRepository {
    suspend fun getArticles(query: String?, sources: List<String>): List<ArticleDto>
}