package space.idan.fidonews.datasource.network

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.request.get
import io.ktor.http.*
import space.idan.fidonews.BuildConfig
import space.idan.fidonews.datasource.network.model.TopHeadlinesResponse
import space.idan.fidonews.datasource.network.model.TopHeadlinesSourcesResponse

class NewsApiImpl(
    private val client: HttpClient,
) : NewsApi {
    override suspend fun getTopHeadlinesSources(): TopHeadlinesSourcesResponse {
        return client.get("/top-headlines/sources")
    }

    override suspend fun getTopHeadlines(q: String?, sources: List<String>): TopHeadlinesResponse {
        return client.get("/top-headlines?sources=${sources.joinToString(",")}")
    }
}