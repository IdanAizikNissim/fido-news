package space.idan.fidonews.datasource.network.model

import kotlinx.serialization.Serializable

@Serializable
data class SourceDto(
    val id: String,
    val name: String,
)