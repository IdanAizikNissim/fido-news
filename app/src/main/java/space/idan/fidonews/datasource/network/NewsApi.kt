package space.idan.fidonews.datasource.network

import space.idan.fidonews.datasource.network.model.TopHeadlinesResponse
import space.idan.fidonews.datasource.network.model.TopHeadlinesSourcesResponse

interface NewsApi {
    suspend fun getTopHeadlinesSources(): TopHeadlinesSourcesResponse
    suspend fun getTopHeadlines(q: String?, sources: List<String>): TopHeadlinesResponse
}