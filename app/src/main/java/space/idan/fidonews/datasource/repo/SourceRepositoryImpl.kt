package space.idan.fidonews.datasource.repo

import space.idan.fidonews.datasource.network.NewsApi
import space.idan.fidonews.datasource.network.model.SourceDto

class SourceRepositoryImpl(
    private val api: NewsApi,
) : SourceRepository {
    override suspend fun getSources(): List<SourceDto> {
        return api.getTopHeadlinesSources().sources
    }
}