package space.idan.fidonews.datasource.repo

import space.idan.fidonews.datasource.network.NewsApi
import space.idan.fidonews.datasource.network.model.ArticleDto

class ArticleRepositoryImpl(
    private val api: NewsApi
) : ArticleRepository {
    override suspend fun getArticles(query: String?, sources: List<String>): List<ArticleDto> {
        return api.getTopHeadlines(query, sources).articles
    }
}