package space.idan.fidonews.datasource.network.client

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*

class HttpClientBuilder {
    private var baseUrl: String? = null
    private var apiToken: String? = null

    fun setBaseUrl(url: String) = apply { this@apply.baseUrl = url }
    fun setApiToken(token: String) = apply { this@apply.apiToken = token }

    fun build(): HttpClient {
        assert(baseUrl != null)
        assert(apiToken != null)

        return HttpClient(Android) {
            install(JsonFeature) {
                serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                    ignoreUnknownKeys = true
                    prettyPrint = false
                })
            }

            defaultRequest {
                url {
                    protocol = URLProtocol.HTTPS
                    host = baseUrl!!
                }
                header(HttpHeaders.ContentType, ContentType.Application.Json)
                header(HttpHeaders.Authorization, apiToken)
            }
        }
    }
}