package space.idan.fidonews.datasource.network.model

import kotlinx.serialization.Serializable

@Serializable
data class TopHeadlinesSourcesResponse(
    val sources: List<SourceDto>
)
