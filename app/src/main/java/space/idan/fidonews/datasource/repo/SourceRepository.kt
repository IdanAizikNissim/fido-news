package space.idan.fidonews.datasource.repo

import space.idan.fidonews.datasource.network.model.SourceDto

interface SourceRepository {
    suspend fun getSources(): List<SourceDto>
}
