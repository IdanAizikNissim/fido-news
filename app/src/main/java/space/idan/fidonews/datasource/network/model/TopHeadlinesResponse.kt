package space.idan.fidonews.datasource.network.model

import kotlinx.serialization.Serializable

@Serializable
data class TopHeadlinesResponse(
    val articles: List<ArticleDto>
)
