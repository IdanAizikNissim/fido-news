package space.idan.fidonews.datasource.network.model

import kotlinx.serialization.Serializable

@Serializable
data class ArticleDto(
    val title: String,
    val description: String,
    val publishedAt: String,
    val url: String,
    val source: SourceDto,
)