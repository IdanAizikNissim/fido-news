package space.idan.fidonews.interactors

import org.joda.time.DateTime
import org.ocpsoft.prettytime.PrettyTime
import space.idan.fidonews.datasource.network.model.ArticleDto
import space.idan.fidonews.datasource.repo.ArticleRepository
import space.idan.fidonews.domain.model.Article
import space.idan.fidonews.presentation.ui.theme.Gradient

class GetTopHeadlines(
    private val repository: ArticleRepository,
) {
    suspend operator fun invoke(query: String?, sources: List<String>): List<Article> {
        return repository.getArticles(query, sources)
            .mapIndexed { index, it -> it.toArticle(index) }
            .sortedByDescending { it.dateTime.millis }
    }
}

private fun ArticleDto.toArticle(index: Int): Article {
    val dateTime = DateTime(this.publishedAt)

    return Article(
        icon = Gradient.all[index % Gradient.all.size],
        title = this.title,
        description = this.description,
        source = this.source.name,
        url = this.url,
        dateTime = dateTime,
        publishedAt = PrettyTime().format(dateTime.toDate())
    )
}