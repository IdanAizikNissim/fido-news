package space.idan.fidonews.interactors

import space.idan.fidonews.datasource.repo.SourceRepository
import space.idan.fidonews.domain.model.Source

class GetSources(
    private val repository: SourceRepository
) {
    suspend operator fun invoke(): List<Source> {
        return repository.getSources().map {
            Source(
                id = it.id,
                name = it.name,
                isInclude = false,
            )
        }
    }
}